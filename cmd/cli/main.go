package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	"gitlab.com/forpelevin/go-telnet/internal/cli"
	"gitlab.com/forpelevin/go-telnet/internal/tcp"
)

var (
	timeout = flag.Int("timeout", 60, "timeout for a connection in seconds. Default value is 1 min.")
	host    = flag.String("host", "", "host to connect")
	port    = flag.String("port", "", "port to connect")
)

func main() {
	flag.Parse()

	timeoutDuration := time.Duration(*timeout) * time.Second
	conn, err := tcp.NewConnectionWithTimeout(*host, *port, timeoutDuration)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Connected to %s\n", conn.RemoteAddr())

	// Wait for Control C to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	go func() {
		inputReader := &cli.ConsoleInputReader{}
		tcpWriter := tcp.NewDefaultTCPWriter(conn)
		tcpReader := tcp.NewDefaultTCPReader(conn)
		for {
			fmt.Println("Please type some text to send...")
			msg, err := inputReader.GetInput()
			if err != nil {
				log.Fatal(err)
			}

			fmt.Println("Sending your message...")
			err = tcpWriter.Write(bytes.NewReader(msg))
			if err != nil {
				log.Fatal(err)
			}

			fmt.Println("Reading response from the TCP connection...")
			err = tcpReader.Read(cli.ConsoleOutput{})
			if err != nil {
				log.Fatal(err)
			}
		}
	}()

	// Block until a signal is received
	<-ch
	fmt.Println("Closing connection...")
	err = conn.Close()
	if err != nil {
		log.Fatal(err)
	}

}
