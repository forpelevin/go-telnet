# Go telnet
The utility is a telnet alternative written on Golang.
## Prerequisites
For the successful using you should have:
```
go >= 1.12
```
## Installation
```
go get -u gitlab.com/forpelevin/go-telnet
```
## Sample of using
```
go-telnet -host=192.168.33.100 -port=80 -timeout=60
```
## License
This project is licensed under the MIT License.
