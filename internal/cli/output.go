package cli

import "fmt"

// ConsoleOutput is an abstraction for the CLI output service.
type ConsoleOutput struct{}

// Write writes given bytes as string on a new line in CLI.
func (ConsoleOutput) Write(p []byte) (n int, err error) {
	fmt.Println(string(p))

	return len(p), nil
}
