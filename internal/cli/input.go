package cli

import (
	"bufio"
	"os"
	"strings"
)

// ConsoleInputReader is InputReader that reads from CLI. It scanns user input until he sends 2 line breaks.
type ConsoleInputReader struct{}

// GetInput scanns user input from CLI until he sends 2 line breaks.
func (ir *ConsoleInputReader) GetInput() ([]byte, error) {
	scanner := bufio.NewScanner(os.Stdin)
	builder := strings.Builder{}
	for scanner.Scan() {
		if scanner.Text() == "" {
			return []byte(builder.String()), nil
		}

		builder.WriteString(scanner.Text())
		builder.WriteString("\n")
	}

	return nil, scanner.Err()
}
