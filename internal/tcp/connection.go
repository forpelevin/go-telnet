package tcp

import (
	"fmt"
	"log"
	"net"
	"time"
)

const dialTimeoutSeconds = 3

// NewConnectionWithTimeout creates a new TCP connection with provided timeout.
// It returns an error in case of failed dialing.
func NewConnectionWithTimeout(host, port string, timeoutDuration time.Duration) (net.Conn, error) {
	dialer := net.Dialer{Timeout: dialTimeoutSeconds * time.Second}
	conn, err := dialer.Dial("tcp", fmt.Sprintf("%s:%s", host, port))
	if err != nil {
		return nil, err
	}

	// Set timeout for the connection.
	err = conn.SetDeadline(time.Now().Add(timeoutDuration))
	if err != nil {
		log.Fatal(err)
	}

	return conn, nil
}
