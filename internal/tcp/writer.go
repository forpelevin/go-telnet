package tcp

import (
	"io"
	"net"
)

// DefaultTCPWriter
type DefaultTCPWriter struct {
	conn net.Conn
}

// NewDefaultTCPWriter creates a new DefaultTCPWriter instance
func NewDefaultTCPWriter(conn net.Conn) *DefaultTCPWriter {
	return &DefaultTCPWriter{conn: conn}
}

// Write writes data to a TCP connection from the readFrom io.Reader.
func (tcpWriter *DefaultTCPWriter) Write(readFrom io.Reader) error {
	_, err := io.Copy(tcpWriter.conn, readFrom)

	return err
}
