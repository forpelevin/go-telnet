package tcp

import (
	"bufio"
	"io"
	"net"
	"strings"
)

// DefaultTCPReader is a common TCP reader implementation.
type DefaultTCPReader struct {
	conn net.Conn
}

// NewDefaultTCPReader creates a new DefaultTCPReader instance.
func NewDefaultTCPReader(conn net.Conn) *DefaultTCPReader {
	return &DefaultTCPReader{conn: conn}
}

// Read reads data from a TCP connection (tcpReader.conn) to the readTo io.Writer.
func (tcpReader *DefaultTCPReader) Read(readTo io.Writer) error {
	scanner := bufio.NewScanner(tcpReader.conn)
	for scanner.Scan() {
		_, err := io.Copy(readTo, strings.NewReader(scanner.Text()))
		if err != nil {
			return err
		}
	}

	return scanner.Err()
}
